<?php

namespace Dropkick\Core\Container\Definition\ExpressionFactory;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\ExpressionFactoryInterface;

/**
 * Class ExpressionFactory.
 *
 * A generic implementation of ExpressionFactoryInterface that does nothing.
 */
class ExpressionFactory implements ExpressionFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function calculate($expression, ContainerInterface $container) {
    // The default expression factory does not calculate anything.
    return NULL;
  }

}
