<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ServiceInterface.
 *
 * The base definition for a service.
 */
interface ServiceInterface {

  /**
   * Get the interface or class the service is providing.
   *
   * @return string
   *   The service interface/class.
   */
  public function getInterface();

  /**
   * Get the class the service uses for initialization.
   *
   * @return string
   *   The service class.
   */
  public function getClass();

  /**
   * Get all the parameters for the service.
   *
   * @return \Dropkick\Core\Container\Definition\ParameterInterface[]
   *   The service constructor parameters.
   */
  public function getParameters();

  /**
   * Get the list of tags applied to the service.
   *
   * @return \Dropkick\Core\Container\Definition\TagInterface[]
   *   The service tags.
   */
  public function getTags();

  /**
   * Confirm that a tag has been applied to the service.
   *
   * @param string $tag
   *   The tag.
   *
   * @return bool
   *   Confirm the tag.
   */
  public function hasTag($tag);

  /**
   * Get the tag.
   *
   * @param string $tag
   *   The tag.
   *
   * @return \Dropkick\Core\Container\Definition\TagInterface|null
   *   The tag or NULL.
   */
  public function getTag($tag);

  /**
   * Get the dependencies for the service.
   *
   * @return string[]
   *   The service dependencies.
   */
  public function getDependencies();

  /**
   * Get the priority of the service.
   *
   * @return int
   *   The priority level.
   */
  public function getPriority();

  /**
   * Get the status of the lazy-loading for the service.
   *
   * @return bool
   *   Confirmation the service is lazy-loaded.
   */
  public function isLazyLoaded();

  /**
   * Get the definition of the service.
   *
   * @return array
   *   The service definition.
   */
  public function getDefinition();

}
