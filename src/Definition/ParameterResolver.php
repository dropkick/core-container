<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\ContainerInterface;

/**
 * Class ParameterResolver.
 *
 * A generic implementation of ParameterResolverInterface.
 */
class ParameterResolver implements ParameterResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Dropkick\Core\Container\Definition\ParameterResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Add a resolver.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterResolverInterface $resolver
   *   A resolver object.
   *
   * @return static
   *   The resolver object.
   */
  public function addResolver(ParameterResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ParameterInterface $parameter) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($parameter)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(ParameterInterface $parameter, ContainerInterface $container) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($parameter)) {
        return $resolver->getParameter($parameter, $container);
      }
    }
    return NULL;
  }

}
