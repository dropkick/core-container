<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface DecoratorInterface.
 *
 * Allow a service to act as a decorator within the ContainerInterface.
 */
interface DecoratorInterface {

  /**
   * Confirm the service is another service decorator.
   *
   * @return bool
   *   Confirmation the service is a decorator.
   */
  public function isDecorator();

  /**
   * Get the service this service decorates.
   *
   * @return string
   *   The decorated service.
   */
  public function getDecorates();

}
