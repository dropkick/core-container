<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\ContainerInterface;

/**
 * Interface ExpressionFactoryInterface.
 *
 * Provides the means to convert a mathematical expression into a value.
 */
interface ExpressionFactoryInterface {

  /**
   * Calculate the parameter based on the expression.
   *
   * @param string $expression
   *   The expression to evaluate.
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container for the service.
   *
   * @return mixed
   *   The calculated value.
   */
  public function calculate($expression, ContainerInterface $container);

}
