<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ServiceDiscoveryInterface.
 *
 * Allow service discover to locate services to be passed to the container
 * definition.
 */
interface ServiceDiscoveryInterface {

  /**
   * Return all service definitions.
   *
   * @return array[]
   *   An array of service definitions indexed by service interface or class.
   */
  public function getServices();

  /**
   * Return service definitions that are tagged.
   *
   * @param string $tag
   *   The tag to filter the services.
   *
   * @return array[]
   *   An array of service definitions indexed by service interface or class.
   */
  public function getTaggedServices($tag);

}
