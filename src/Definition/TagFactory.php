<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class TagFactory.
 *
 * A generic implementation of TagFactoryInterface.
 */
class TagFactory implements TagFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createTag(array $definition) {
    return new Tag($definition);
  }

}
