<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface TagInterface.
 *
 * Provides configurable tag information for services.
 */
interface TagInterface {

  /**
   * Get the tag identifier.
   *
   * @return string
   *   The tag.
   */
  public function getTag();

  /**
   * Get all the tag configuration.
   *
   * @return array
   *   The configuration.
   */
  public function getConfig();

  /**
   * Get the definition of the tag.
   *
   * @return array
   *   The tag definition.
   */
  public function getDefinition();

}
