<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class ContainerDefinition.
 *
 * A generic implementation of ContainerDefinitionInterface.
 */
class ContainerDefinition implements ContainerDefinitionInterface {

  /**
   * The service definitions.
   *
   * @var \Dropkick\Core\Container\Definition\ServiceInterface[]
   */
  protected $services = [];

  /**
   * ContainerDefinition constructor.
   *
   * @param array $definition
   *   The container definition.
   */
  public function __construct(array $definition = []) {
    $this->setInterfaces($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getInterface($interface) {
    if (array_key_exists($interface, $this->services)) {
      $service = $this->services[$interface];
      return $service->getDefinition();
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getInterfaces() {
    return array_keys($this->services);
  }

  /**
   * Set an interface configuration.
   *
   * @param string $interface
   *   The interface.
   * @param \Dropkick\Core\Container\Definition\ServiceInterface|null $service
   *   The configuration, or NULL to remove.
   *
   * @return static
   */
  public function setInterface($interface, ServiceInterface $service = NULL) {
    if (is_null($service)) {
      unset($this->services[$interface]);
    }
    else {
      $this->services[$interface] = $service;
    }
    return $this;
  }

  /**
   * Set multiple interface configurations.
   *
   * @param array $interfaces
   *   The interface definitions, indexed by interface name.
   *
   * @return static
   */
  public function setInterfaces(array $interfaces) {
    foreach ($interfaces as $interface => $config) {
      $this->setInterface($interface, $config);
    }
    return $this;
  }

}
