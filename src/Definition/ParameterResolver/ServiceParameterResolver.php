<?php

namespace Dropkick\Core\Container\Definition\ParameterResolver;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\ParameterInterface;
use Dropkick\Core\Container\Definition\ParameterResolverInterface;

/**
 * Class ServiceParameterResolver.
 *
 * A resolver that converts service names from the container.
 */
class ServiceParameterResolver implements ParameterResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(ParameterInterface $parameter) {
    return $parameter->getType() === 'service';
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(ParameterInterface $parameter, ContainerInterface $container) {
    if ($parameter->getValue() === ContainerInterface::class) {
      return $container;
    }
    return $container->get($parameter->getValue());
  }

}
