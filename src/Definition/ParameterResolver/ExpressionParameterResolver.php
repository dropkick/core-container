<?php

namespace Dropkick\Core\Container\Definition\ParameterResolver;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\ExpressionFactoryInterface;
use Dropkick\Core\Container\Definition\ParameterInterface;
use Dropkick\Core\Container\Definition\ParameterResolverInterface;
use Dropkick\Core\Container\Exception\ContainerException;
use Dropkick\Core\Formattable\FormattableString;

/**
 * Class ExpressionParameterResolver.
 *
 * Applies expression calculation for service parameters.
 */
class ExpressionParameterResolver implements ParameterResolverInterface {

  /**
   * The expression factory.
   *
   * @var \Dropkick\Core\Container\Definition\ExpressionFactoryInterface
   */
  protected $factory;

  /**
   * ExpressionParameterResolver constructor.
   *
   * @param \Dropkick\Core\Container\Definition\ExpressionFactoryInterface $factory
   *   The expression factory.
   */
  public function __construct(ExpressionFactoryInterface $factory) {
    $this->factory = $factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ParameterInterface $parameter) {
    return $parameter->getType() === 'expression';
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(ParameterInterface $parameter, ContainerInterface $container) {
    $expression = $parameter->getValue();
    try {
      $value = $this->factory->calculate($expression, $container);
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      throw new ContainerException(
        FormattableString::create(
          'Expression "{{ expression }}" failed to be calculated "{{ message }}".',
          [
            'expression' => $expression,
            'message' => $message,
          ]
        )
      );
    }
    return $value;
  }

}
