<?php

namespace Dropkick\Core\Container\Definition\ParameterResolver;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\ParameterInterface;
use Dropkick\Core\Container\Definition\ParameterResolverInterface;

/**
 * Class ConstantParameterResolver.
 *
 * Provides a constant parameter value.
 */
class ConstantParameterResolver implements ParameterResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(ParameterInterface $parameter) {
    return $parameter->getType() === 'constant';
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(ParameterInterface $parameter, ContainerInterface $container) {
    return $parameter->getValue();
  }

}
