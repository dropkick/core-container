<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ServiceFactoryInterface.
 *
 * Allows for creating services.
 */
interface ServiceFactoryInterface {

  /**
   * Create a service.
   *
   * @param string $interface
   *   The service interface.
   * @param array $definition
   *   The service definition.
   *
   * @return \Dropkick\Core\Container\Definition\ServiceInterface
   *   A service object.
   */
  public function createService($interface, array $definition);

  /**
   * Sort the services in order of priority highest to lowest.
   *
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $a
   *   A service object.
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $b
   *   A service object.
   *
   * @return int
   *   -1 if $a < $b, 0 if $a == $b, 1 if $a > $b
   */
  public function sortServices(ServiceInterface $a, ServiceInterface $b);

}
