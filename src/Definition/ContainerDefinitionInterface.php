<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ContainerDefinitionInterface.
 *
 * Provides a generic set of definitions that are expected within the
 * ContainerInterface.
 */
interface ContainerDefinitionInterface {

  /**
   * Get the configuration for a given interface.
   *
   * @param string $interface
   *   The interface.
   *
   * @return array
   *   The configuration for a given interface.
   */
  public function getInterface($interface);

  /**
   * Get the priority stored list of interfaces.
   *
   * @return string[]
   *   The ordered list of interfaces defined by the definition.
   */
  public function getInterfaces();

}
