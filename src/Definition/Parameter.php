<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class Parameter.
 *
 * A generic implementation of ParameterInterface.
 */
class Parameter implements ParameterInterface {

  /**
   * The type of the parameter.
   *
   * @var string
   */
  protected $type;

  /**
   * The value of the parameter.
   *
   * @var string
   */
  protected $value;

  /**
   * The value of the definition.
   *
   * @var string
   */
  protected $definition;

  /**
   * Parameter constructor.
   *
   * @param string $type
   *   The service parameter definition type.
   * @param mixed $value
   *   The service parameter definition value.
   * @param string $definition
   *   The service parameter definition.
   */
  public function __construct($type, $value, $definition) {
    $this->type = $type;
    $this->value = $value;
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    return $this->definition;
  }

}
