<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Trait TaggedServiceDiscoveryTrait.
 *
 * Provide the hasTag functionality for a tagged service discovery class.
 */
trait TaggedServiceDiscoveryTrait {

  /**
   * Check the service has a tag defined.
   *
   * @param array $service
   *   The service definition.
   * @param string $tag
   *   The tag definition.
   *
   * @return bool
   *   Confirm the service supports the tag.
   */
  protected function hasTag(array $service, $tag) {
    if (isset($service['tags'])) {
      foreach ($service['tags'] as $service_tags) {
        if (isset($service_tags['name']) && $service_tags['name'] === $tag) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
