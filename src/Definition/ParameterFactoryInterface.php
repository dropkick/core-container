<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ParameterFactoryInterface.
 *
 * Allows for service parameters to be created for resolution.
 */
interface ParameterFactoryInterface {

  /**
   * Confirm the parameter factory.
   *
   * @param string $parameter
   *   The parameter handler.
   *
   * @return bool
   *   Confirmation the parameter can be created.
   */
  public function applies($parameter);

  /**
   * Create an parameter.
   *
   * @param string $parameter
   *   The parameter value.
   *
   * @return \Dropkick\Core\Container\Definition\ParameterInterface|null
   *   The parameter object.
   */
  public function createParameter($parameter);

}
