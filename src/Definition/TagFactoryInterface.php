<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface TagFactoryInterface.
 *
 * Allows for the creation of tags for services.
 */
interface TagFactoryInterface {

  /**
   * Create a tag.
   *
   * @param array $definition
   *   The tag definition.
   *
   * @return \Dropkick\Core\Container\Definition\TagInterface
   *   The tag object.
   */
  public function createTag(array $definition);

}
