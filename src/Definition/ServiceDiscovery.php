<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class ServiceDiscovery.
 *
 * A generic implementation of the ServiceDiscoveryInterface.
 */
class ServiceDiscovery implements ServiceDiscoveryInterface {

  /**
   * The discoverers used to locate services.
   *
   * @var \Dropkick\Core\Container\Definition\ServiceDiscoveryInterface[]
   */
  protected $discoveries = [];

  /**
   * Add a discovery mechanism to the service discovery.
   *
   * @param \Dropkick\Core\Container\Definition\ServiceDiscoveryInterface $discovery
   *   A service discovery object.
   *
   * @return static
   *   The service discovery object.
   */
  public function addDiscovery(ServiceDiscoveryInterface $discovery) {
    $this->discoveries[] = $discovery;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getServices() {
    $services = [];
    foreach ($this->discoveries as $discovery) {
      $services += $discovery->getServices();
    }
    return $services;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaggedServices($tag) {
    $services = [];
    foreach ($this->discoveries as $discovery) {
      $services += $discovery->getTaggedServices($tag);
    }
    return $services;
  }

}
