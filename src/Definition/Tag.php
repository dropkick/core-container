<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\Exception\UndefinedTagException;

/**
 * Class Tag.
 *
 * Provides a way for service definitions to be collected and processed.
 */
class Tag implements TagInterface {
  /**
   * The tag identifier.
   *
   * @var string
   */
  protected $tag;

  /**
   * The extra tag configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * Tag constructor.
   *
   * @param array $tag
   *   The tag definition.
   *
   * @throws \Dropkick\Core\Container\Exception\UndefinedTagException
   */
  public function __construct(array $tag) {
    if (empty($tag['name'])) {
      throw new UndefinedTagException();
    }

    // Store the tag that is being applied.
    $this->tag = (string) $tag['name'];
    unset($tag['name']);

    // Add the additional tag configuration.
    $this->config = $tag;
  }

  /**
   * {@inheritdoc}
   */
  public function getTag() {
    return $this->tag;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Set a configuration item for a tag.
   *
   * @param string $id
   *   The tag config identifier.
   * @param mixed|null $value
   *   The value for the identifier.
   *
   * @return static
   *   The tag object.
   */
  public function setConfig($id, $value = NULL) {
    if (is_null($value)) {
      unset($this->config[$id]);
    }
    else {
      $this->config[$id] = $value;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    return [
      'name' => $this->getTag(),
    ] + $this->getConfig();
  }

}
