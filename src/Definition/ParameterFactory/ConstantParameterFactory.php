<?php

namespace Dropkick\Core\Container\Definition\ParameterFactory;

use Dropkick\Core\Container\Definition\Parameter;
use Dropkick\Core\Container\Definition\ParameterFactoryInterface;

/**
 * Class ConstantParameterFactory.
 *
 * Treats a service parameter definition as a constant.
 */
class ConstantParameterFactory implements ParameterFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function applies($parameter) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createParameter($parameter) {
    return new Parameter('constant', $parameter, $parameter);
  }

}
