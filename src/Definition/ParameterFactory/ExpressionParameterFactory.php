<?php

namespace Dropkick\Core\Container\Definition\ParameterFactory;

use Dropkick\Core\Container\Definition\Parameter;
use Dropkick\Core\Container\Definition\ParameterFactoryInterface;

/**
 * Class ExpressionParameterFactory.
 */
class ExpressionParameterFactory implements ParameterFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function applies($parameter) {
    return is_string($parameter) && strpos($parameter, '%') === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function createParameter($parameter) {
    return new Parameter('expression', substr($parameter, 1), $parameter);
  }

}
