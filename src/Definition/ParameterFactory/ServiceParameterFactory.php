<?php

namespace Dropkick\Core\Container\Definition\ParameterFactory;

use Dropkick\Core\Container\Definition\Parameter;
use Dropkick\Core\Container\Definition\ParameterFactoryInterface;

/**
 * Class ServiceParameterFactory.
 *
 * Treats a service parameter definition as a service.
 */
class ServiceParameterFactory implements ParameterFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function applies($parameter) {
    return is_string($parameter) && strpos($parameter, '@') === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function createParameter($parameter) {
    return new Parameter('service', substr($parameter, 1), $parameter);
  }

}
