<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Interface ParameterInterface.
 *
 * Allows for parameters to be passed through to service constructors.
 */
interface ParameterInterface {

  /**
   * Return the type of parameter.
   *
   * @return string
   *   The parameter type.
   */
  public function getType();

  /**
   * Return the value for the parameter.
   *
   * @return string
   *   The parameter value.
   */
  public function getValue();

  /**
   * Get the definition of the parameter.
   *
   * @return string
   *   The parameter.
   */
  public function getDefinition();

}
