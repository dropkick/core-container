<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class ParameterFactory.
 *
 * A generic implementation of ParameterFactoryInterface.
 */
class ParameterFactory implements ParameterFactoryInterface {

  /**
   * The factories used for generating service parameter definitions.
   *
   * @var \Dropkick\Core\Container\Definition\ParameterFactoryInterface[]
   */
  protected $factories = [];

  /**
   * Add a factory.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterFactoryInterface $factory
   *   A factory object.
   *
   * @return static
   *   The factory object.
   */
  public function addFactory(ParameterFactoryInterface $factory) {
    $this->factories[] = $factory;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($parameter) {
    foreach ($this->factories as $factory) {
      if ($factory->applies($parameter)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createParameter($parameter) {
    foreach ($this->factories as $factory) {
      if ($factory->applies($parameter)) {
        return $factory->createParameter($parameter);
      }
    }
    return NULL;
  }

}
