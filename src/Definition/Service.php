<?php

namespace Dropkick\Core\Container\Definition;

/**
 * Class Service.
 *
 * A generic implementation of ServiceInterface.
 */
class Service implements ServiceInterface, DecoratorInterface {

  /**
   * The interface or class the service represents.
   *
   * @var string
   */
  protected $interface;

  /**
   * The class that is initialized for the service.
   *
   * @var string
   */
  protected $class;

  /**
   * The interface or class the service decorates.
   *
   * @var string
   */
  protected $decorates;

  /**
   * The parameter values that will be passed to the service on initialization.
   *
   * @var \Dropkick\Core\Container\Definition\ParameterInterface[]
   */
  protected $parameters = [];

  /**
   * The list of dependencies the service requires.
   *
   * @var bool[]
   *   The index represents the class/interface, and the value the state
   *   of its dependency requirement.
   */
  protected $dependencies = [];

  /**
   * The tags that have been applied to the service.
   *
   * @var \Dropkick\Core\Container\Definition\TagInterface[]
   */
  protected $tags = [];

  /**
   * The priority level of the service.
   *
   * The higher the priority of the service, the earlier it is initialized.
   *
   * @var int
   */
  protected $priority;

  /**
   * The service can be lazy-loaded.
   *
   * @var bool
   */
  protected $lazy;

  /**
   * Service constructor.
   *
   * @param string $interface
   *   The class name or interface that the service represents.
   */
  public function __construct($interface) {
    $this->interface = $interface;
    $this->class = $interface;
  }

  /**
   * {@inheritdoc}
   */
  public function getInterface() {
    return $this->interface;
  }

  /**
   * Set the class to be used to initialize the service.
   *
   * @param string $class
   *   The class used for the service.
   *
   * @return static
   *   The service object.
   */
  public function setClass($class) {
    $this->class = $class;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass() {
    return $this->class;
  }

  /**
   * Set the parameters used by the service.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterInterface[] $parameters
   *   The parameters for the service as strings.
   *
   * @return static
   *   The service object.
   */
  public function setParameters(array $parameters) {
    $this->parameters = [];
    foreach ($parameters as $index => $parameter) {
      $this->setParameter($index, $parameter);
    }
    return $this;
  }

  /**
   * Sets an parameter by index.
   *
   * @param int $index
   *   The index to apply the update to the parameter.
   * @param \Dropkick\Core\Container\Definition\ParameterInterface|null $parameter
   *   Sets the parameter, or removes it when NULL.
   *
   * @return static
   *   The service object.
   */
  public function setParameter($index, ParameterInterface $parameter = NULL) {
    if (is_null($parameter)) {
      unset($this->parameters[$index]);
    }
    else {
      $this->parameters[$index] = $parameter;
    }
    $this->parameters = array_values($this->parameters);
    return $this;
  }

  /**
   * Get the parameter at a given index.
   *
   * @param int $index
   *   The index.
   *
   * @return \Dropkick\Core\Container\Definition\ParameterInterface
   *   The parameter.
   */
  public function getParameter($index) {
    return $this->parameters[$index];
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Set the tags for the service.
   *
   * @param \Dropkick\Core\Container\Definition\TagInterface[] $tags
   *   The tags.
   *
   * @return static
   *   The service object.
   */
  public function setTags(array $tags) {
    foreach ($tags as $tag) {
      $this->setTag($tag);
    }
    return $this;
  }

  /**
   * Set a tag for the service.
   *
   * @param \Dropkick\Core\Container\Definition\TagInterface $tag
   *   A tag object.
   *
   * @return static
   *   The service object.
   */
  public function setTag(TagInterface $tag) {
    $this->tags[$tag->getTag()] = $tag;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->tags;
  }

  /**
   * Remove a tag from the service.
   *
   * @param string $tag
   *   A tag.
   *
   * @return static
   *   The service object.
   */
  public function removeTag($tag) {
    unset($this->tags[$tag]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTag($tag) {
    return array_key_exists($tag, $this->tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getTag($tag) {
    if (array_key_exists($tag, $this->tags)) {
      return $this->tags[$tag];
    }
    return NULL;
  }

  /**
   * Confirm the service uses a dependency.
   *
   * @param string $dependency
   *   The service dependency.
   *
   * @return bool
   *   Confirmation of dependency.
   */
  public function hasDependency($dependency) {
    $deps = $this->getDependencies();
    return in_array($dependency, $deps);
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies() {
    $deps = [];
    foreach ($this->getParameters() as $arg) {
      if ($arg->getType() === 'service') {
        $deps[] = $arg->getValue();
      }
    }
    return array_unique($deps);
  }

  /**
   * Set the priority level of the service.
   *
   * The higher the priority level, the closer to the top of the service list.
   *
   * @param int $priority
   *   The priority level.
   *
   * @return static
   *   The service object.
   */
  public function setPriority($priority) {
    $this->priority = (int) $priority;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority() {
    return $this->priority;
  }

  /**
   * {@inheritdoc}
   */
  public function isDecorator() {
    return isset($this->decorates);
  }

  /**
   * Set the service this service decorates.
   *
   * @param string $decorates
   *   The service to decorate.
   *
   * @return static
   *   The service object.
   */
  public function setDecorates($decorates) {
    $this->decorates = (string) $decorates;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDecorates() {
    return $this->decorates;
  }

  /**
   * {@inheritdoc}
   */
  public function isLazyLoaded() {
    return !isset($this->lazy) || $this->lazy;
  }

  /**
   * Set the service as being lazy-loaded.
   *
   * @param bool $lazy
   *   Flag to set lazy-loading.
   *
   * @return static
   *   The service object.
   */
  public function setLazyLoaded($lazy) {
    $this->lazy = (bool) $lazy;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    $service = [
      'class' => $this->getClass(),
    ];

    // Get the parameter definition.
    if ($args = $this->getParameters()) {
      $service['arguments'] = array_map(function (Parameter $arg) {
        return $arg->getDefinition();
      }, $args);
    }

    // Get the tag definitions.
    if ($tags = $this->getTags()) {
      $service['tags'] = array_map(function (Tag $tag) {
        return $tag->getDefinition();
      }, array_values($tags));
    }

    // Get the decoration behaviour.
    if ($decorates = $this->getDecorates()) {
      $service['decorates'] = $decorates;
    }

    // Get the priority level.
    if ($priority = $this->getPriority()) {
      $service['priority'] = $priority;
    }

    // Get the laziness of the service.
    if (!$this->isLazyLoaded()) {
      $service['lazy'] = FALSE;
    }

    return $service;
  }

  /**
   * Sort the services by priority.
   *
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $a
   *   Service to compare.
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $b
   *   Service to compare.
   *
   * @return int
   *   -1 means $a comes before $b, 0 means do not change order,
   *   1 means $b comes before $a.
   */
  public static function sort(ServiceInterface $a, ServiceInterface $b) {
    return $b->getPriority() <=> $a->getPriority();
  }

}
