<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\Exception\ContainerException;
use Dropkick\Core\Formattable\FormattableString;

/**
 * Class ServiceFactory.
 *
 * A generic implementation of ServiceFactoryInterface.
 */
class ServiceFactory implements ServiceFactoryInterface {

  /**
   * The parameter factory for conversion of service parameters.
   *
   * @var \Dropkick\Core\Container\Definition\ParameterFactoryInterface
   */
  protected $parameterFactory;

  /**
   * The tag factory for conversion of service tags.
   *
   * @var \Dropkick\Core\Container\Definition\TagFactoryInterface
   */
  protected $tagFactory;

  /**
   * ServiceFactory constructor.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterFactoryInterface $parameter_factory
   *   The factory generating the parameters.
   * @param \Dropkick\Core\Container\Definition\TagFactoryInterface $tag_factory
   *   The factory generating the tags.
   */
  public function __construct(ParameterFactoryInterface $parameter_factory, TagFactoryInterface $tag_factory) {
    $this->parameterFactory = $parameter_factory;
    $this->tagFactory = $tag_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function createService($interface, array $definition) {
    $service = (new Service($interface))
      ->setClass(!empty($definition['class']) ? $definition['class'] : $interface)
      ->setPriority(!empty($definition['priority']) ? $definition['priority'] : 0)
      ->setLazyLoaded(!isset($definition['lazy']) || $definition['lazy']);

    // Add the parameters.
    if (!empty($definition['arguments']) && is_array($definition['arguments'])) {
      $parameters = [];
      foreach ($definition['arguments'] as $key => $parameter) {
        if ($this->parameterFactory->applies($parameter)) {
          $parameters[$key] = $this->parameterFactory->createParameter($parameter);
        }
        else {
          throw new ContainerException(
            FormattableString::create(
              'Unable to locate a parameter factory for "{{ parameter }}" for interface "{{ interface }}".',
              [
                'parameter' => $key,
                'interface' => $interface,
              ]
            )
          );
        }
      }
      $service->setParameters($parameters);
    }

    // Add the tags.
    if (!empty($definition['tags']) && is_array($definition['tags'])) {
      $tags = [];
      foreach ($definition['tags'] as $key => $tag) {
        $tags[$key] = $this->tagFactory->createTag($tag);
      }
      $service->setTags($tags);
    }

    // Decorate an existing service.
    if (!empty($definition['decorates']) && is_string($definition['decorates'])) {
      $service->setDecorates($definition['decorates']);
    }

    return $service;
  }

  /**
   * {@inheritdoc}
   */
  public function sortServices(ServiceInterface $a, ServiceInterface $b) {
    return Service::sort($a, $b);
  }

}
