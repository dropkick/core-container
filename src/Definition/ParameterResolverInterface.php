<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\ContainerInterface;

/**
 * Interface ParameterResolverInterface.
 *
 * Allows parameters to be resolved for a service definition.
 */
interface ParameterResolverInterface {

  /**
   * Confirm that the resolver handles the specific parameter.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterInterface $parameter
   *   The parameter object.
   *
   * @return bool
   *   Confirmation the resolver applies.
   */
  public function applies(ParameterInterface $parameter);

  /**
   * Convert an parameter into a value for use with the container.
   *
   * @param \Dropkick\Core\Container\Definition\ParameterInterface $parameter
   *   The parameter.
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   *
   * @return mixed
   *   The parameter value.
   */
  public function getParameter(ParameterInterface $parameter, ContainerInterface $container);

}
