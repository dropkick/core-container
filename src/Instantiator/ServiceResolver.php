<?php

namespace Dropkick\Core\Container\Instantiator;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Invokable\ArgumentInterface;
use Dropkick\Core\Invokable\ResolverInterface;

/**
 * Class ServiceResolver.
 *
 * A resolver for service instantiation.
 */
class ServiceResolver implements ResolverInterface {

  /**
   * The container object.
   *
   * @var \Dropkick\Core\Container\ContainerInterface
   */
  protected $container;

  /**
   * Any arguments that precede the container arguments.
   *
   * @var array
   */
  protected $arguments = [];

  /**
   * ServiceResolver constructor.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ArgumentInterface $argument) {
    return count($this->arguments) > $argument->getPosition();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ArgumentInterface $argument) {
    return $this->arguments[$argument->getPosition()];
  }

  /**
   * Set any prefix argument when applying decorations.
   *
   * @param array $arguments
   *   The arguments for the service.
   *
   * @return static
   *   The resolver object.
   */
  public function setArguments(array $arguments) {
    $this->arguments = $arguments;
    return $this;
  }

}
