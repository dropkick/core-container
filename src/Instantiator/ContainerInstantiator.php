<?php

namespace Dropkick\Core\Container\Instantiator;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Injection\ContainerInjectionInterface;
use Dropkick\Core\Container\Injection\CreationInjectionInterface;
use Dropkick\Core\Container\Injection\StaticInjectionInterface;
use Dropkick\Core\Instantiator\Instantiator;
use Dropkick\Core\Instantiator\InstantiatorInterface;
use Dropkick\Core\Instantiator\InstantiatorTrait;

/**
 * Class ContainerInstantiator.
 *
 * An implementation of InstantiatorInterface that targets common container
 * based instantiation patterns.
 */
class ContainerInstantiator implements InstantiatorInterface {
  use InstantiatorTrait;

  /**
   * The container object.
   *
   * @var \Dropkick\Core\Container\ContainerInterface
   */
  protected $container;

  /**
   * ContainerInstantiator constructor.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container used for initialization.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * Instantiate a class.
   *
   * @param string $class
   *   The class name.
   * @param string|null $requirement
   *   The class or interface the object must match.
   *
   * @return object|string|null
   *   Returns an object for a class, string when class is static, or NULL when
   *   the class is unsupported.
   *
   * @throws \Dropkick\Core\Instantiator\InstantiationException
   */
  public function instantiate($class, $requirement = NULL) {
    // Creation via container.
    if (is_subclass_of($class, CreationInjectionInterface::class)) {
      $callable = [$class, 'create'];
      return is_callable($callable) ?
        call_user_func_array($callable, [$this->container]) :
        NULL;
    }

    // Static only service.
    if (is_subclass_of($class, StaticInjectionInterface::class)) {
      $callable = [$class, 'inject'];
      if (is_callable($callable)) {
        call_user_func_array($callable, [$this->container]);
      }
      return $class;
    }

    // Container aware service.
    if (is_subclass_of($class, ContainerInjectionInterface::class)) {
      /** @var \Dropkick\Core\Container\Injection\ContainerInjectionInterface $object */
      $object = (new Instantiator())
        ->setReflector($this->getReflector())
        ->setResolver($this->getResolver())
        ->instantiate($class, $requirement);
      $object->setContainer($this->container);
      return $object;
    }

    return NULL;
  }

}
