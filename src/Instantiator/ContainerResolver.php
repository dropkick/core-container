<?php

namespace Dropkick\Core\Container\Instantiator;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Invokable\ArgumentInterface;
use Dropkick\Core\Invokable\ResolverInterface;

/**
 * Class ContainerResolver.
 *
 * The container resolution.
 */
class ContainerResolver implements ResolverInterface {

  /**
   * The container used for resolution.
   *
   * @var \Dropkick\Core\Container\ContainerInterface
   */
  protected $container;

  /**
   * ContainerResolver constructor.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ArgumentInterface $argument) {
    $type = $argument->getType();
    return $type && $this->container->has($type);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ArgumentInterface $argument) {
    return $this->container->get($argument->getType());
  }

}
