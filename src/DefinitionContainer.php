<?php

namespace Dropkick\Core\Container;

use Dropkick\Core\Container\Definition\DecoratorInterface;
use Dropkick\Core\Container\Definition\ContainerDefinitionInterface;
use Dropkick\Core\Container\Definition\ParameterResolverInterface;
use Dropkick\Core\Container\Definition\TaggedServiceDiscoveryTrait;
use Dropkick\Core\Container\Exception\ContainerException;
use Dropkick\Core\Container\Exception\NotFoundException;
use Dropkick\Core\Container\Instantiator\ContainerInstantiator;
use Dropkick\Core\Container\Instantiator\ServiceResolver;
use Dropkick\Core\Container\Definition\ServiceFactoryInterface;
use Dropkick\Core\Container\Definition\ServiceInterface;
use Dropkick\Core\Container\Definition\TagInterface;
use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Instantiator\InstantiationException;
use Dropkick\Core\Instantiator\Instantiator;
use Dropkick\Core\Instantiator\InstantiatorInterface;

/**
 * Class CoreContainer.
 *
 * @package Dropkick\Core\Container\Container
 */
class DefinitionContainer implements ContainerInterface {
  use TaggedServiceDiscoveryTrait;

  /**
   * The services that are provided by the container.
   *
   * @var \Dropkick\Core\Container\Definition\Service[]|object[]|string[]
   */
  protected $services = [];

  /**
   * Map the services which are decorators for others.
   *
   * @var string[][]
   */
  protected $decorators = [];

  /**
   * The service ids that are in the process of loading.
   *
   * @var array
   */
  protected $loading = [];

  /**
   * The instantiator used for initializing an object.
   *
   * @var \Dropkick\Core\Instantiator\InstantiatorInterface
   */
  protected $instantiator;

  /**
   * The resolver used when instantiating objects.
   *
   * @var \Dropkick\Core\Container\Instantiator\ServiceResolver
   */
  protected $serviceResolver;

  /**
   * The service factory for creating services.
   *
   * @var \Dropkick\Core\Container\Definition\ServiceFactoryInterface
   */
  protected $serviceFactory;

  /**
   * The argument factory.
   *
   * @var \Dropkick\Core\Container\Definition\ParameterResolverInterface
   */
  protected $parameterResolver;

  /**
   * The definition of the container.
   *
   * @var \Dropkick\Core\Container\Definition\ContainerDefinitionInterface
   */
  protected $definition;

  /**
   * Container constructor.
   *
   * @param \Dropkick\Core\Instantiator\InstantiatorInterface $instantiator
   *   The instantiator object.
   * @param \Dropkick\Core\Container\Definition\ContainerDefinitionInterface $definition
   *   The builder providing the configuration for the Container.
   * @param \Dropkick\Core\Container\Definition\ParameterResolverInterface $parameter_resolver
   *   The argument factory used for converting arguments to values.
   * @param \Dropkick\Core\Container\Definition\ServiceFactoryInterface $service_factory
   *   The service factory that creates the services for the container.
   */
  public function __construct(
    InstantiatorInterface $instantiator,
    ContainerDefinitionInterface $definition,
    ParameterResolverInterface $parameter_resolver,
    ServiceFactoryInterface $service_factory
  ) {
    $this->definition = $definition;
    $this->parameterResolver = $parameter_resolver;
    $this->serviceFactory = $service_factory;
    $this->serviceResolver = new ServiceResolver($this);
    $this->instantiator = (new Instantiator())
      ->setResolver($this->serviceResolver)
      ->addInstantiator(new ContainerInstantiator($this))
      ->addInstantiator($instantiator);

    // Add all the services.
    foreach ($definition->getInterfaces() as $interface) {
      $this->addService($service_factory->createService($interface, $definition->getInterface($interface)));
    }

    // Sort the priority order of the services.
    uasort($this->services, [$service_factory, 'sortServices']);

    // Reorders and filters decorators that can be applied to the services
    // based on priority.
    $services = array_keys($this->services);
    foreach ($this->decorators as $interface => $decorators) {
      $this->decorators[$interface] = array_intersect($services, $decorators);
    }

    // Load all the services which are not lazy-loaded.
    foreach ($services as $id) {
      // Service has not yet been loaded, so check it is lazy-load.
      if ($this->services[$id] instanceof ServiceInterface && !$this->services[$id]->isLazyLoaded()) {
        $this->get($id);
      }
    }
  }

  /**
   * Adds a service to the list of the container.
   *
   * This should not be called after the initial processing, as this will affect
   * the service decoration layer.
   *
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $service
   *   The service to add.
   */
  protected function addService(ServiceInterface $service) {
    $this->services[$service->getInterface()] = $service;

    // Add service to the decorators option.
    if ($service instanceof DecoratorInterface && $service->isDecorator()) {
      $this->decorators[$service->getDecorates()][] = $service->getInterface();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($id) {
    return $this->getService($id, []);
  }

  /**
   * Get the decorated or undecorated versions of the service.
   *
   * @param string $id
   *   The service identifier.
   * @param array $decoration
   *   The decoration values.
   *
   * @return \Dropkick\Core\Container\Definition\ServiceInterface|mixed
   *   The service object.
   *
   * @throws \Dropkick\Core\Container\Exception\NotFoundException
   *   Triggered when request an undefined service.
   */
  protected function getService($id, array $decoration = []) {
    // The service does not exist.
    if (!array_key_exists($id, $this->services)) {
      throw new NotFoundException(
        FormattableString::create(
          'No service found with id "{{ interface }}".',
          ['interface' => $id]
        )
      );
    }

    // We are yet to initialize the service.
    if ($this->services[$id] instanceof ServiceInterface) {
      // Just load the base class.
      $this->services[$id] = $this->loadService($id, $decoration);
    }

    // Save the service we are planning to retrieve.
    $service = $this->services[$id];

    // Cycle through the decorators from highest to lowest priority.
    if (isset($this->decorators[$id])) {
      $previous = $service;

      // The load service takes care of the decoration behaviour.
      foreach ($this->decorators[$id] as $decorator) {
        $service = $this->getService($decorator, [$previous]);
        $previous = $service;
      }
    }

    return $service;
  }

  /**
   * {@inheritdoc}
   */
  public function has($id) {
    return array_key_exists($id, $this->services);
  }

  /**
   * Loads the service using the behaviour expected.
   *
   * @param string $id
   *   The service identifier.
   * @param object[] $decoration
   *   The decorator to be used with instantiation.
   *
   * @return mixed
   *   The service object.
   *
   * @throws \Dropkick\Core\Container\Exception\ContainerException
   */
  protected function loadService($id, array $decoration = []) {
    // Ensure no circular dependencies while loading the services.
    if (in_array($id, $this->loading)) {
      throw new ContainerException(
        FormattableString::create(
          'Circular dependency involving "{{ interface }}".',
          ['interface' => $id]
        )
      );
    }

    // Push the service as being involved in the loading process.
    array_push($this->loading, $id);

    /** @var \Dropkick\Core\Container\Definition\Service $service */
    $service = $this->services[$id];

    // Parameter factory needs to be passed to the arguments.
    $arguments = $this->getArguments($service, $decoration);

    // Set any decorations being used as arguments.
    $this->serviceResolver->setArguments($arguments);

    // Create object from class.
    try {
      $object = $this->instantiator->instantiate($service->getClass(), $service->getInterface());
    }
    catch (InstantiationException $e) {
      throw new ContainerException(
        FormattableString::create(
          'Unable to instantiate "{{ class }}"',
          ['class' => $service->getClass()]
        )
      );
    }

    // Reset any decorations being used as arguments.
    $this->serviceResolver->setArguments([]);

    // Overwrite the existing service definition.
    $this->services[$id] = $object;

    // Remove the service from the loading process.
    array_pop($this->loading);

    // The service is a collection, so add all the services to the collection.
    if ($collector = $service->getTag('service.collector')) {
      $this->addCollection($object, $collector);
    }

    // Return the service by identifier.
    return $this->services[$id];
  }

  /**
   * Get the arguments for the creation of the service.
   *
   * @param \Dropkick\Core\Container\Definition\ServiceInterface $service
   *   The service to be created.
   * @param array $decoration
   *   Any decorated objects to be passed as arguments.
   *
   * @return array
   *   The arguments as an array.
   *
   * @throws \Dropkick\Core\Container\Exception\ContainerException
   */
  protected function getArguments(ServiceInterface $service, array $decoration) {
    // Get the service arguments, that need to be resolved.
    $service_args = $service->getParameters();

    // Create the argument list.
    $args = [];

    // Apply an prefix decorations, and assume they take up the arguments of
    // the service.
    foreach ($decoration as $decorator) {
      $args[] = $decorator;
      array_shift($service_args);
    }

    // Load all the arguments based on the argument type.
    $factory = $this->parameterResolver;
    foreach ($service_args as $argument) {
      if ($factory->applies($argument)) {
        $args[] = $factory->getParameter($argument, $this);
      }
      else {
        throw new ContainerException(
          FormattableString::create(
            'Unable to resolve argument "{{ argument }}" for service "{{ service }}".',
            [
              'argument' => $argument->getDefinition(),
              'service' => $service->getInterface(),
            ]
          )
        );
      }
    }

    return $args;
  }

  /**
   * Get services that have been tagged with a collection.
   *
   * @param string $collection
   *   The collection tag.
   *
   * @return array
   *   The interfaces of the services that provide the collection tag.
   */
  protected function getTaggedServices($collection) {
    $services = [];
    foreach ($this->definition->getInterfaces() as $interface) {
      $service = $this->definition->getInterface($interface);
      if ($this->hasTag($service, $collection)) {
        $services[] = $interface;
      }
    }
    return array_intersect(array_keys($this->services), $services);
  }

  /**
   * Adds the service.collection information to a service.
   *
   * @param object|string $object
   *   The service object.
   * @param \Dropkick\Core\Container\Definition\TagInterface $tag
   *   The tag object.
   */
  protected function addCollection($object, TagInterface $tag) {
    $config = $tag->getConfig();
    $method = $config['method'] ?? FALSE;
    if (!$method || !method_exists($object, $method)) {
      return;
    }

    // Get all the tagged services that matching the collection.
    $collection = $config['tag'] ?? FALSE;
    if (!$collection || !($services = $this->getTaggedServices($collection))) {
      return;
    }

    // Cycle through each tagged service adding to the collection.
    foreach ($services as $interface) {
      $service = $this->get($interface);
      $object->$method($service);
    }
  }

}
