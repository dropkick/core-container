<?php

namespace Dropkick\Core\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class NotFoundException.
 *
 * Triggered when a service cannot find the appropriate identifier.
 */
class NotFoundException extends \Exception implements NotFoundExceptionInterface {
}
