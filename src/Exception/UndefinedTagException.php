<?php

namespace Dropkick\Core\Container\Exception;

/**
 * Class UndefinedTagException.
 *
 * Triggered when the tag definition does not include the tag array key.
 */
class UndefinedTagException extends \Exception {
}
