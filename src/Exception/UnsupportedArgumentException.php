<?php

namespace Dropkick\Core\Container\Exception;

/**
 * Class UnsupportedArgumentException.
 *
 * Triggered when the ArgumentFactoryInterface has an argument it does not
 * understand how to convert.
 */
class UnsupportedArgumentException extends \Exception {
}
