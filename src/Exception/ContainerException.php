<?php

namespace Dropkick\Core\Container\Exception;

use Psr\Container\ContainerExceptionInterface;

/**
 * Class ContainerException.
 *
 * Triggered whenever there is an error within the container.
 */
class ContainerException extends \Exception implements ContainerExceptionInterface {
}
