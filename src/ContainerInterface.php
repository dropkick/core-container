<?php

namespace Dropkick\Core\Container;

use Psr\Container\ContainerInterface as PsrContainerInterface;

/**
 * Interface ContainerInterface.
 *
 * An extension to the underlying PSR Container interface that
 * may allow containers to be exported to file.
 */
interface ContainerInterface extends PsrContainerInterface {
}
