<?php

namespace Dropkick\Core\Container;

/**
 * Class Container.
 *
 * Converts a ContainerBuilderInterface into a fully functional container that
 * provides singletons of each service. Factory behaviour should be provided
 * by a service.
 */
class Container implements ContainerInterface {

  /**
   * The containers that provide the services.
   *
   * @var array
   */
  protected $containers = [];

  /**
   * Add the container interface.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   *
   * @return static
   */
  public function addContainer(ContainerInterface $container) {
    $this->containers[] = $container;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function get($id) {
    foreach ($this->containers as $container) {
      if ($container->has($id)) {
        return $container->get($id);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function has($id) {
    foreach ($this->containers as $container) {
      if ($container->has($id)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
