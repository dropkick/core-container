<?php

namespace Dropkick\Core\Container\Injection;

use Dropkick\Core\Container\ContainerInterface;

/**
 * Interface ContainerInjectionInterface.
 *
 * Provides for setting the container on an object after construction.
 */
interface ContainerInjectionInterface {

  /**
   * Set the dependency injection container.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   *
   * @return static
   *   The service object.
   */
  public function setContainer(ContainerInterface $container);

}
