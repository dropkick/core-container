<?php

namespace Dropkick\Core\Container\Injection;

use Dropkick\Core\Container\ContainerInterface;

/**
 * Interface StaticInjectionInterface.
 *
 * Some class contain static functions which may require the container to
 * initialize. This interface allows the to be initialized without requiring
 * instantiation.
 */
interface StaticInjectionInterface {

  /**
   * Allows static initialization of values.
   *
   * @param \Dropkick\Core\Container\ContainerInterface $container
   *   The container object.
   */
  public static function inject(ContainerInterface $container);

}
