<?php

namespace Dropkick\Core\Container;

/**
 * Class MemoryContainer.
 *
 * An implementation using standard memory caching.
 */
class MemoryContainer implements ContainerInterface {

  /**
   * The container services.
   *
   * @var array
   */
  protected $services = [];

  /**
   * MemoryContainer constructor.
   *
   * @param array $services
   *   The service listing.
   */
  public function __construct(array $services = []) {
    foreach ($services as $interface => $service) {
      $this->set($interface, $service);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($id) {
    if (array_key_exists($id, $this->services)) {
      return $this->services[$id];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function has($id) {
    return array_key_exists($id, $this->services);
  }

  /**
   * Set the service matching the interface.
   *
   * @param string $id
   *   The service identifier.
   * @param mixed $service
   *   The service object.
   *
   * @return static
   *   The container object.
   */
  public function set($id, $service) {
    $this->services[$id] = $service;
    return $this;
  }

}
