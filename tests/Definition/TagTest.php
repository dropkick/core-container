<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\Exception\UndefinedTagException;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase {

  public function testDefinition() {
    $definition = ['name' => 'a_new_tag', 'config' => 'value', 'random' => 'random'];
    $tag = new Tag($definition);

    $this->assertEquals('a_new_tag', $tag->getTag());
    $this->assertArrayNotHasKey('name', $tag->getConfig());
    $this->assertArrayHasKey('config', $tag->getConfig());
    $this->assertArrayHasKey('random', $tag->getConfig());
    $this->assertEquals($definition, $tag->getDefinition());
  }

  public function testInvalid() {
    $this->expectException(UndefinedTagException::class);
    $tag = new Tag([]);
  }

  public function testSetConfig() {
    $definition = ['name' => 'a_new_tag', 'config' => 'value', 'random' => 'random'];
    $tag = new Tag($definition);

    $tag->setConfig('config', NULL);
    $tag->setConfig('random', 'config');
    $tag->setConfig('newbie', 'newbie');
    
    $this->assertArrayNotHasKey('config', $tag->getConfig());
    $this->assertArrayHasKey('newbie', $tag->getConfig());
    $this->assertArrayHasKey('random', $tag->getConfig());
    $this->assertEquals('config', $tag->getConfig()['random']);
  }

}
