<?php

namespace Dropkick\Core\Container\Definition\Service;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Injection\StaticInjectionInterface;

class TestStaticService implements StaticInjectionInterface {
  protected static $service;

  public static function inject(ContainerInterface $container) {
    self::$service = $container->get(TestConstantService::class)->constants;
  }

  public static function getService() {
    return self::$service;
  }

}
