<?php

namespace Dropkick\Core\Container\Definition\Service;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Injection\ContainerInjectionInterface;

class TestInjectionService implements ContainerInjectionInterface {

  public $constant;
  public $expression;
  public $constants;
  public $service;

  public function __construct($constant, $expression, TestConstantService $service) {
    $this->constant = $constant;
    $this->expression = $expression;
    $this->service = $service;
  }

  public function setContainer(ContainerInterface $container) {
    $this->constants = $container->get(TestConstantService::class)->constants;
  }

}