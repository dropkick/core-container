<?php

namespace Dropkick\Core\Container\Definition\Service;

class TestConstantService  {

  public $constants = [];

  public function __construct(
    $a1 = NULL,
    $a2 = NULL,
    $a3 = NULL,
    $a4 = NULL,
    $a5 = NULL,
    $a6 = NULL,
    $a7 = NULL,
    $a8 = NULL,
    $a9 = NULL,
    $a10 = NULL,
    $a11 = NULL,
    $a12 = NULL,
    $a13 = NULL,
    $a14 = NULL,
    $a15 = NULL,
    $a16 = NULL,
    $a17 = NULL,
    $a18 = NULL,
    $a19 = NULL,
    $a20 = NULL
  ) {
    $this->constants = array_filter(func_get_args());
  }

}
