<?php

namespace Dropkick\Core\Container\Definition\Service;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Injection\CreationInjectionInterface;

class TestCreationService implements CreationInjectionInterface {

  public $constants = [];

  public function __construct($constants) {
    $this->constants = $constants;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get(TestConstantService::class)->constants);
  }

}
