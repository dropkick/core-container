<?php

namespace Dropkick\Core\Container\Definition\Service;

use Dropkick\Core\Container\Definition\TestServiceInterface;

class TestCollectionService  {

  public $services = [];

  public function addService(TestServiceInterface $service) {
    $this->services[] = $service;
    return $this;
  }

}
