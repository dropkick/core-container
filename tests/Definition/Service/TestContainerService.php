<?php

namespace Dropkick\Core\Container\Definition\Service;

use Dropkick\Core\Container\ContainerInterface;

class TestContainerService {

  public $container;

  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

}
