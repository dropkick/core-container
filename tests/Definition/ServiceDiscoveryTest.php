<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\Definition\ServiceDiscovery\TestServiceDiscovery;
use PHPUnit\Framework\TestCase;

class ServiceDiscoveryTest extends TestCase {

  public function testServiceDiscovery() {
    $discovery = new ServiceDiscovery();

    // Create a test discovery mechanism.
    $test_discovery = new TestServiceDiscovery();
    $test_discovery->services = [
      'dummy' => [
        'class' => 'test',
        'tags' => [['name' => 'tagged']],
      ],
    ];
    $discovery->addDiscovery($test_discovery);

    $services = $discovery->getServices();
    $this->assertEquals(1, count($services));

    $services = $discovery->getTaggedServices('untagged');
    $this->assertEquals(0, count($services));

    $services = $discovery->getTaggedServices('tagged');
    $this->assertEquals(1, count($services));
  }

}