<?php

namespace Dropkick\Core\Container\Definition;

use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase {

  public function testMinimal() {
    $service = new Service(self::class);

    $this->assertEquals(self::class, $service->getInterface());
    $this->assertEquals(self::class, $service->getClass());
    $this->assertEquals([], $service->getParameters());
    $this->assertEquals([], $service->getTags());
    $this->assertEquals(0, $service->getPriority());
    $this->assertEquals(false, $service->isDecorator());
    $this->assertEquals('', $service->getDecorates());
    $this->assertEquals([], $service->getDependencies());
    $this->assertEquals(true, $service->isLazyLoaded());
    $this->assertEquals(['class' => self::class], $service->getDefinition());
  }

  public function testMaximum() {
    $definition = [
      'class' => 'test',
      'arguments' => ['@dependency', '%2 + 4', 'strict value'],
      'tags' => [['name' => 'only'], ['name' => 'one', 'config' => 'random']],
      'decorates' => 'text',
      'priority' => 10,
      'lazy' => FALSE,
    ];
    $service = new Service(self::class);
    $service->setClass($definition['class'])
      ->setParameters([
        new Parameter('service', 'dependency', $definition['arguments'][0]),
        new Parameter('expression', '2 + 4', $definition['arguments'][1]),
        new Parameter('constant', 'strict value', $definition['arguments'][2]),
      ])
      ->setTags([
        new Tag($definition['tags'][0]),
        new Tag($definition['tags'][1]),
      ])
      ->setDecorates($definition['decorates'])
      ->setPriority($definition['priority'])
      ->setLazyLoaded($definition['lazy']);

    $this->assertEquals(self::class, $service->getInterface());
    $this->assertEquals($definition['class'], $service->getClass());
    $this->assertEquals(10, $service->getPriority());
    $this->assertEquals(true, $service->isDecorator());
    $this->assertEquals($definition['decorates'], $service->getDecorates());
    $this->assertEquals(['dependency'], $service->getDependencies());
    $this->assertEquals(false, $service->isLazyLoaded());
    $this->assertEquals($definition, $service->getDefinition());

    $this->assertTrue($service->hasDependency('dependency'));
    $this->assertFalse($service->hasDependency('depend'));

    $args = $service->getParameters();
    $this->assertEquals(3, count($args));
    $this->assertEquals('service', $service->getParameter(0)->getType());
    $this->assertEquals('dependency', $service->getParameter(0)->getValue());
    $service->setParameter(0);
    $this->assertEquals('expression', $service->getParameter(0)->getType());
    $this->assertEquals('2 + 4', $service->getParameter(0)->getValue());
    $this->assertEquals('constant', $service->getParameter(1)->getType());

    $tags = $service->getTags();
    $this->assertEquals(2, count($tags));
    $this->assertEquals(true, $service->hasTag('one'));

    $service->removeTag('one');
    $this->assertEquals(false, $service->hasTag('one'));
  }

  public function testSort() {
    $services = [
      (new Service('b'))->setPriority(1),
      (new Service('a'))->setPriority(10),
      (new Service('c'))->setPriority(1),
    ];

    usort($services, Service::class . '::sort');
    $this->assertEquals('a', $services[0]->getInterface());
    $this->assertEquals('b', $services[1]->getInterface());
    $this->assertEquals('c', $services[2]->getInterface());
  }
}
