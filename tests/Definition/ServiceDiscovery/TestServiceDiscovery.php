<?php

namespace Dropkick\Core\Container\Definition\ServiceDiscovery;

use Dropkick\Core\Container\Definition\ServiceDiscoveryInterface;
use Dropkick\Core\Container\Definition\TaggedServiceDiscoveryTrait;

class TestServiceDiscovery implements ServiceDiscoveryInterface {
  use TaggedServiceDiscoveryTrait;

  public $services = [];

  public function getServices() {
    return $this->services;
  }

  public function getTaggedServices($tag) {
    $services = [];
    foreach ($this->services as $service) {
      if ($this->hasTag($service, $tag)) {
        $services[] = $service;
      }
    }
    return $services;
  }

}