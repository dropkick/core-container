<?php

namespace Dropkick\Core\Container\Definition;

use PHPUnit\Framework\TestCase;

class ContainerDefinitionTest extends TestCase {

  public function testMinimal() {
    $definition = new ContainerDefinition();

    $this->assertEquals(0, count($definition->getInterfaces()));
  }

  public function testConstructor() {
    $services = [
      'test' => new Service('test')
    ];

    $definition = new ContainerDefinition($services);

    $this->assertEquals(1, count($definition->getInterfaces()));
    $this->assertEmpty($definition->getInterface('undefined'));
    $this->assertEquals(['class' => 'test'], $definition->getInterface('test'));

    $definition->setInterface('test');

    $this->assertEquals(0, count($definition->getInterfaces()));
  }

}
