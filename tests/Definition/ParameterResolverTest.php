<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\MemoryContainer;
use PHPUnit\Framework\TestCase;

class ParameterResolverTest extends TestCase {
  
  public function testUnhandledParameter() {
    $parameter = new Parameter('constant', 'constant', 'constant');
    $resolver = new ParameterResolver();
    $container = new MemoryContainer();
    
    $this->assertNull($resolver->getParameter($parameter, $container));
  }

}
