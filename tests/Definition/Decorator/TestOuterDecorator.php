<?php

namespace Dropkick\Core\Container\Definition\Decorator;


use Dropkick\Core\Container\Definition\Service\TestConstantService;

class TestOuterDecorator extends TestConstantService {

  public $service;

  public function __construct(TestConstantService $service) {
    $this->service = $service;
  }

}