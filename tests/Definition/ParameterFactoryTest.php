<?php

namespace Dropkick\Core\Container\Definition;

use PHPUnit\Framework\TestCase;

class ParameterFactoryTest extends TestCase {

  public function testUnhandledParameter() {
    $factory = new ParameterFactory();
    $this->assertNull($factory->createParameter(''));
  }

}
