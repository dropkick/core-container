<?php

namespace Dropkick\Core\Container\Definition;

class TestDefinition implements ContainerDefinitionInterface {

  public $map = [];
  public $interfaces = [];
  public $decorated = [];

  public function getMap() {
    return $this->map;
  }

  public function getMappedService($service_id) {
    return array_search($service_id, $this->map);
  }

  public function setMapping($interface, $id = NULL) {
    if (is_null($id)) {
      unset($this->map[$interface]);
    }
    else {
      $this->map[$interface] = $id;
    }
    return $this;
  }

  public function getMapping($interface) {
    return $this->map[$interface];
  }

  public function getInterface($interface) {
    return $this->interfaces[$interface];
  }

  public function getInterfaces() {
    return array_keys($this->interfaces);
  }

  public function getDecorated() {
    return array_keys($this->decorated);
  }

  public function getDecorators($interface) {
    return $this->decorated[$interface];
  }

  public function setDecorator($interface, $decorator) {
    $this->decorated[$interface][] = $decorator;
    $this->decorated[$interface] = array_unique($this->decorated[$interface]);
    return $this;
  }

  public function setInterface($interface, array $config = NULL) {
    $this->interfaces[$interface] = $config;
    return $this;
  }

  public function getTaggedServices($tag) {
    $services = [];
    foreach ($this->interfaces as $interface => $config) {
      if (isset($config['tags'])) {
        foreach ($config['tags'] as $item) {
          if ($item['name'] === $tag) {
            $services[] = $interface;
          }
        }
      }
    }
    return $services;
  }
}