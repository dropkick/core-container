<?php

namespace Dropkick\Core\Container\Definition\ExpressionFactory;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\Service;
use PHPUnit\Framework\TestCase;

class ExpressionFactoryTest extends TestCase {

  public function testNull() {
    $service = $this->getMockBuilder(Service::class)
      ->disableOriginalConstructor()
      ->getMock();
    $container = $this->getMockBuilder(ContainerInterface::class)
      ->getMock();

    $factory = new ExpressionFactory();
    $this->assertNull($factory->calculate('', $container));
  }

}
