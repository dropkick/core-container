<?php

namespace Dropkick\Core\Container\Definition\ExpressionFactory;

use Dropkick\Core\Container\ContainerInterface;
use Dropkick\Core\Container\Definition\ExpressionFactoryInterface;

class TestExpressionFactory implements ExpressionFactoryInterface {
  public function calculate($expression, ContainerInterface $container) {
    if ($expression === 'invalid') {
      throw new \Exception('invalid exception');
    }
    return 6;
  }

}