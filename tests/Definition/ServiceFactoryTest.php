<?php

namespace Dropkick\Core\Container\Definition;

use Dropkick\Core\Container\Definition\Service\TestConstantService;
use Dropkick\Core\Container\Exception\ContainerException;
use PHPUnit\Framework\TestCase;

class ServiceFactoryTest extends TestCase {

  public function testInvalidParameter() {
    $parameterFactory = new ParameterFactory();
    $service = [
      'class' => TestConstantService::class,
      'arguments' => ['constant']
    ];

    $factory = new ServiceFactory($parameterFactory, new TagFactory());

    $this->expectException(ContainerException::class);
    $factory->createService(TestConstantService::class, $service);
  }

}
