<?php

namespace Dropkick\Core\Container;

use Dropkick\Core\Container\Definition\Decorator\TestInnerDecorator;
use Dropkick\Core\Container\Definition\Decorator\TestOuterDecorator;
use Dropkick\Core\Container\Definition\ExpressionFactory\TestExpressionFactory;
use Dropkick\Core\Container\Definition\ParameterFactory;
use Dropkick\Core\Container\Definition\ParameterResolver;
use Dropkick\Core\Container\Definition\Service\TestCollectionService;
use Dropkick\Core\Container\Definition\Service\TestCollectorService;
use Dropkick\Core\Container\Definition\Service\TestConstantService;
use Dropkick\Core\Container\Definition\Service\TestContainerService;
use Dropkick\Core\Container\Definition\Service\TestCreationService;
use Dropkick\Core\Container\Definition\Service\TestInjectionService;
use Dropkick\Core\Container\Definition\Service\TestStaticService;
use Dropkick\Core\Container\Definition\ServiceFactory;
use Dropkick\Core\Container\Definition\TagFactory;
use Dropkick\Core\Container\Definition\TestDefinition;
use Dropkick\Core\Container\Exception\ContainerException;
use Dropkick\Core\Container\Exception\NotFoundException;
use Dropkick\Core\Instantiator\Instantiator;
use PHPUnit\Framework\TestCase;

class DefinitionContainerTest extends TestCase {

  protected $instantiator;

  /**
   * Create a container with some sane defaults.
   *
   * @param \Dropkick\Core\Container\Definition\ContainerDefinitionInterface $builder
   *   The container definition object.
   * @param array $options
   *   The additional options when creating the container.
   *
   * @return \Dropkick\Core\Container\DefinitionContainer
   */
  protected function getContainer($builder, $options = []) {
    $factory = (new ParameterFactory())
      ->addFactory(new ParameterFactory\ServiceParameterFactory())
      ->addFactory(new ParameterFactory\ExpressionParameterFactory())
      ->addFactory(new ParameterFactory\ConstantParameterFactory());
    $serviceFactory = new ServiceFactory($factory, new TagFactory());
    $resolver = (new ParameterResolver())
      ->addResolver(new ParameterResolver\ServiceParameterResolver())
      ->addResolver(new ParameterResolver\ExpressionParameterResolver(
        new TestExpressionFactory()
      ))
      ->addResolver(new ParameterResolver\ConstantParameterResolver());
    return new DefinitionContainer(
      $this->instantiator,
      $builder,
      $resolver,
      $serviceFactory
    );
  }

  public function setUp(): void {
    $this->instantiator = new Instantiator();
  }

  public function testEmptyContainer() {
    $builder = new TestDefinition();
    $container = $this->getContainer($builder);
    $this->assertEquals(false, $container->has('test'));
  }

  public function providerArgClasses() {
    $values = [];
    for ($i = 0; $i < 20; $i++) {
      $values[] = [
        'class' => TestConstantService::class,
        'arguments' => $i ? range(1, $i) : [],
      ];
    }
    return $values;
  }

  /**
   * @dataProvider providerArgClasses
   */
  public function testMultipleArguments($class, $arguments) {
    $builder = new TestDefinition();

    // Define the service.
    $builder->interfaces[$class] = [
      'class' => $class,
      'arguments' => $arguments,
    ];

    $container = $this->getContainer($builder);
    $service = $container->get($class);
    $this->assertEquals($arguments, $service->constants);
  }

  public function testStaticService() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestStaticService::class] = [
      'class' => TestStaticService::class,
    ];
    $container = $this->getContainer($builder);
    $service = $container->get(TestStaticService::class);
    $this->assertEquals(TestStaticService::class, $service);
    $this->assertEquals(['constant'], TestStaticService::getService());
  }

  public function testNonLazyService() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestStaticService::class] = [
      'class' => TestStaticService::class,
      'lazy' => FALSE,
    ];
    $container = $this->getContainer($builder);
    $this->assertEquals(['constant'], TestStaticService::getService());
  }

  public function testCreationService() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestCreationService::class] = [
      'class' => TestCreationService::class,
    ];
    $container = $this->getContainer($builder);
    $service = $container->get(TestCreationService::class);
    $this->assertEquals(['constant'], $service->constants);
  }

  public function testInjectService() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestInjectionService::class] = [
      'class' => TestInjectionService::class,
      'arguments' => ['random', '%2 + 4', '@' . TestConstantService::class],
    ];
    $container = $this->getContainer($builder);
    $constant = $container->get(TestConstantService::class);
    $service = $container->get(TestInjectionService::class);
    $this->assertEquals(['constant'], $service->constants);
    $this->assertEquals($constant, $service->service);
    $this->assertEquals(6, $service->expression);
    $this->assertEquals('random', $service->constant);
  }

  public function testDecorators() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestOuterDecorator::class] = [
      'class' => TestOuterDecorator::class,
      'decorates' => TestConstantService::class,
      'priority' => 5,
    ];
    $builder->interfaces[TestInnerDecorator::class] = [
      'class' => TestInnerDecorator::class,
      'decorates' => TestConstantService::class,
      'priority' => 10,
    ];
    $container = $this->getContainer($builder);
    $service = $container->get(TestConstantService::class);
    $this->assertEquals(TestOuterDecorator::class, get_class($service));
    $service = $service->service;
    $this->assertEquals(TestInnerDecorator::class, get_class($service));
    $service = $service->service;
    $this->assertEquals(TestConstantService::class, get_class($service));
  }

  public function testInvalidService() {
    $builder = new TestDefinition();
    $container = $this->getContainer($builder);
    $this->expectException(NotFoundException::class);
    $service = $container->get(TestConstantService::class);
  }
  
  public function testCircular() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestInjectionService::class] = [
      'class' => TestInjectionService::class,
      'arguments' => ['random', '%2 + 4', '@' . TestInjectionService::class],
    ];
    $container = $this->getContainer($builder);
    $this->expectException(ContainerException::class);
    $service = $container->get(TestInjectionService::class);
  }

  public function testUnsupported() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestInjectionService::class,
      'arguments' => ['constant'],
    ];
    $container = $this->getContainer($builder);
    $this->expectException(ContainerException::class);
    $service = $container->get(TestConstantService::class);
  }

  public function testInvalidExpression() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'class' => TestConstantService::class,
      'arguments' => ['constant'],
    ];
    $builder->interfaces[TestInjectionService::class] = [
      'class' => TestInjectionService::class,
      'arguments' => ['random', '%invalid', '@' . TestConstantService::class],
    ];
    $container = $this->getContainer($builder);
    $this->expectException(ContainerException::class);
    $service = $container->get(TestInjectionService::class);
  }

  public function testGetSelf() {
    $builder = new TestDefinition();
    $container = $this->getContainer($builder);
    $object = $container->has(ContainerInterface::class);
    $this->assertFalse($object);
  }

  public function testSelfArgument() {
    $builder = new TestDefinition();
    $builder->interfaces[TestContainerService::class] = [
      'arguments' => ['@' . ContainerInterface::class],
    ];
    $container = $this->getContainer($builder);
    $service = $container->get(TestContainerService::class);
    $this->assertEquals($container, $service->container);
  }

  public function testCollector() {
    $builder = new TestDefinition();
    $builder->interfaces[TestCollectionService::class] = [
      'class' => TestCollectionService::class,
      'tags' => [
        ['name' => 'service.collector', 'method' => 'addService', 'tag' => 'test.collector']
      ]
    ];
    $builder->interfaces[TestCollectorService::class] = [
      'tags' => [
        ['name' => 'test.collector']
      ]
    ];
    $container = $this->getContainer($builder);

    $service = $container->get(TestCollectionService::class);
    $this->assertEquals(1, count($service->services));
  }

  public function testInvalidCollector() {
    $builder = new TestDefinition();
    $builder->interfaces[TestCollectionService::class] = [
      'class' => TestCollectionService::class,
      'tags' => [
        ['name' => 'service.collector', 'method' => 'addCollector', 'tag' => 'test.collector']
      ]
    ];
    $builder->interfaces[TestCollectorService::class] = [
      'tags' => [
        ['name' => 'test.collector']
      ]
    ];
    $container = $this->getContainer($builder);

    $service = $container->get(TestCollectionService::class);
    $this->assertEquals(0, count($service->services));
  }

  public function testInvalidTag() {
    $builder = new TestDefinition();
    $builder->interfaces[TestCollectionService::class] = [
      'class' => TestCollectionService::class,
      'tags' => [
        ['name' => 'service.collector', 'method' => 'addService', 'collection' => 'test.collector']
      ]
    ];
    $builder->interfaces[TestCollectorService::class] = [
      'tags' => [
        ['name' => 'test.collector']
      ]
    ];
    $container = $this->getContainer($builder);

    $service = $container->get(TestCollectionService::class);
    $this->assertEquals(0, count($service->services));
  }

  public function testUnhandledArgument() {
    $builder = new TestDefinition();
    $builder->interfaces[TestConstantService::class] = [
      'arguments' => ['constant'],
    ];

    $factory = (new ParameterFactory())
      ->addFactory(new ParameterFactory\ServiceParameterFactory())
      ->addFactory(new ParameterFactory\ExpressionParameterFactory())
      ->addFactory(new ParameterFactory\ConstantParameterFactory());
    $serviceFactory = new ServiceFactory($factory, new TagFactory());
    $resolver = (new ParameterResolver())
      ->addResolver(new ParameterResolver\ServiceParameterResolver())
      ->addResolver(new ParameterResolver\ExpressionParameterResolver(
        new TestExpressionFactory()
      ));
    $container = new DefinitionContainer(
      $this->instantiator,
      $builder,
      $resolver,
      $serviceFactory
    );
    $this->expectException(ContainerException::class);
    $service = $container->get(TestConstantService::class);
  }
}
