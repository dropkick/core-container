<?php

namespace Dropkick\Core\Container;

use Dropkick\Core\Container\Definition\Service\TestConstantService;
use PHPUnit\Framework\TestCase;

class MemoryContainerTest extends TestCase {

  public function testInit() {
    $existing = [
      TestConstantService::class => new TestConstantService()
    ];
    $container = new MemoryContainer($existing);
    $this->assertTrue($container->has(TestConstantService::class));
    $this->assertEquals(
       $existing[TestConstantService::class],
       $container->get(TestConstantService::class)
    );
  }

  public function testCrud() {
    $container = new MemoryContainer();
    $this->assertFalse($container->has(TestConstantService::class));
    $this->assertNull($container->get(TestConstantService::class));
    $service = new TestConstantService();
    $container->set(TestConstantService::class, $service);
    $this->assertTrue($container->has(TestConstantService::class));
    $this->assertEquals($service, $container->get(TestConstantService::class));
  }

}
