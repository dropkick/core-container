<?php

namespace Dropkick\Core\Container;

use Dropkick\Core\Container\Definition\Service\TestConstantService;
use Dropkick\Core\Container\Definition\Service\TestInjectionService;
use Dropkick\Core\Container\Definition\Service\TestStaticService;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase {

  public function testEmpty() {
    $container = new Container();
    $this->assertFalse($container->has(TestConstantService::class));
    $this->assertNull($container->get(TestConstantService::class));
  }

  public function testMultiple() {
    $constant_service = new TestConstantService();
    $static_service = TestStaticService::class;
    $c1 = new MemoryContainer([TestConstantService::class => $constant_service]);
    $c2 = new MemoryContainer([TestStaticService::class => $static_service]);

    $container = (new Container())
      ->addContainer($c1)
      ->addContainer($c2);

    $this->assertFalse($container->has(TestInjectionService::class));
    $this->assertNull($container->get(TestInjectionService::class));

    $this->assertTrue($container->has(TestStaticService::class));
    $this->assertEquals(TestStaticService::class, $container->get(TestStaticService::class));

    $this->assertTrue($container->has(TestConstantService::class));
    $this->assertEquals($constant_service, $container->get(TestConstantService::class));
  }

}
