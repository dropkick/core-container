<?php

namespace Dropkick\Core\Container\Instantiator;

use Dropkick\Core\Container\MemoryContainer;
use Dropkick\Core\Invokable\Argument;
use PHPUnit\Framework\TestCase;

class ContainerResolverTest extends TestCase {

  public function testResolver() {
    $container = new MemoryContainer([
      'test' => 'constant'
    ]);

    $resolver = new ContainerResolver($container);

    $fail = new Argument(0, Argument::class, 'arg', FALSE, NULL);
    $success = new Argument(0, 'test', 'arg', FALSE, NULL);

    $this->assertFalse($resolver->applies($fail));
    $this->assertTrue($resolver->applies($success));
    $this->assertEquals('constant', $resolver->getValue($success));
  }

}
