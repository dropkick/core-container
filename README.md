# Container

A set of interfaces and supporting classes that provide generic 
service container building and factory functionality.

## Getting Started

Use the standard composer require to begin using:
```
composer require dropkick/core-container
```

## Running the tests

The standard tests for this project include phpunit, phpstan and
phpcs. 

```
composer run tests
```

### Unit Tests

This project uses phpunit for testing functionality.

```
vendor/bin/phpunit
```

### Code Checking

This project uses phpstan.

```
vendor/bin/phpstan analyse src --level=6
```

### Code Style

This project uses Drupal coding standards. These are tested using
phpcs.

```
vendor/bin/phpcs --standard=Drupal src
```

## Versioning

This project uses [semver](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/dropkick/core-container/tags). 

## Authors

See the list of [contributors](https://gitlab.com/dropkick/core-container/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

